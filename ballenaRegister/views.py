from django.urls import reverse_lazy

from django.views.generic import (TemplateView, CreateView, DeleteView,
                                  ListView, DetailView)

from django.shortcuts import get_object_or_404

from .models import Animal, Tag
from .forms import AnimalForm, TagForm

# Create your views here.


class Index(TemplateView):
    template_name = "index.html"


class CreateAnimal(CreateView):
    model = Animal
    form_class = AnimalForm
    template_name = "new_animal.html"
    success_url = reverse_lazy("list-animals")


class CreateTag(CreateView):
    form_class = TagForm
    model = Tag
    template_name = "new_tag.html"

    def get_initial(self):
        initial = super(CreateTag, self).get_initial()

        animal_to_tag = self.kwargs.get("animal_id", None)

        if animal_to_tag:
            animal_object = get_object_or_404(Animal, pk=animal_to_tag)
            initial['animal'] = animal_object

        return initial

    def get_success_url(self):
        animal_id = self.kwargs.get("animal_id", None)

        if animal_id:
            self.success_url = reverse_lazy('list-animal-tags',
                                            kwargs={'animal_id': animal_id})
        else:
            self.success_url = reverse_lazy('list-animals')

        return super(CreateTag, self).get_success_url()

    def get_context_data(self, **kwargs):
        context = super(CreateTag, self).get_context_data(**kwargs)
        context['animal_to_tag'] = self.kwargs.get("animal_id", None)

        if context['animal_to_tag']:
            try:
                animal = Animal.objects.get(pk=context['animal_to_tag'])
                context['animal_name'] = animal.animal_tag
            except Animal.DoesNotExist:
                context['animal_name'] = None

        return context


class DeleteAnimal(DeleteView):
    model = Animal
    template_name = "delete_animal.html"
    success_url = reverse_lazy('list-animals')


class DeleteTag(DeleteView):
    model = Tag
    template_name = "delete_tag.html"

    def delete(self, request, *args, **kwargs):
        animal = self.get_object().animal.pk

        self.success_url = reverse_lazy('list-animal-tags',
                                        kwargs={'animal_id': animal})

        return super(DeleteTag, self).delete(request, *args, **kwargs)


class AnimalsListView(ListView):
    model = Animal
    template_name = "animal_list.html"
    context_object_name = "animal_list"


class TagListView(ListView):
    model = Tag
    template_name = "animal_tag_list.html"
    context_object_name = "tag_list"

    def get_queryset(self):
        animal_to_tag = self.kwargs.get("animal_id", None)

        if animal_to_tag:
            animal_object = get_object_or_404(Animal, pk=animal_to_tag)
            queryset = Tag.objects.filter(animal=animal_object).select_related('animal')

        return queryset

    def get_context_data(self, **kwargs):
        context = super(TagListView, self).get_context_data(**kwargs)
        context['animal_id'] = self.kwargs.get("animal_id", None)

        return context


class DetailAnimal(DetailView):
    model = Animal
    template_name = "animal_detail.html"
    context_object_name = "animal"


class DetailTag(DetailView):
    model = Tag
    template_name = "tag_detail.html"
    context_object_name = "tag"
    pk_url_kwarg = 'animal_id'
