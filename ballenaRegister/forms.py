from django import forms
from .models import Animal, Tag


class AnimalForm(forms.ModelForm):
    class Meta:
        model = Animal
        fields = ['animal_tag', 'first_sighting', 'first_latitude', 'first_longitude', 'description', 'thumbnail', ]

    def __init__(self, *args, **kwargs):
        super(AnimalForm, self).__init__(*args, **kwargs)

        self.fields['first_latitude'].widget = forms.HiddenInput()
        self.fields['first_longitude'].widget = forms.HiddenInput()
        self.fields['first_sighting'].widget = forms.HiddenInput()

        self.fields['description'].widget = forms.TextInput()
        self.fields['description'].widget.attrs = {'class': 'form-control', 'placeholder': self.fields['description'].label}

        self.fields['thumbnail'].widget.attrs = {'style': 'display: none;', 'class': 'file'}
        self.fields['thumbnail'].label = "Take photo"

        self.fields['animal_tag'].widget.attrs = {'class': 'form-control', 'placeholder': self.fields['animal_tag'].label, 'style': 'margin-bottom: 5px;'}


class TagForm(forms.ModelForm):
    class Meta:
        model = Tag
        fields = ['animal', 'date_and_time', 'latitude', 'longitude', 'observations', 'thumbnail', ]

    def __init__(self, *args, **kwargs):
        super(TagForm, self).__init__(*args, **kwargs)

        if kwargs['initial'].get("animal", None):
            self.fields['animal'].widget = forms.HiddenInput()
        else:
            self.fields['animal'].widget.attrs = {'class': 'form-control', 'style': 'margin-bottom: 5px;'}

        self.fields['latitude'].widget = forms.HiddenInput()
        self.fields['longitude'].widget = forms.HiddenInput()

        self.fields['thumbnail'].widget.attrs = {'style': 'display: none;', 'class': 'file'}
        self.fields['thumbnail'].label = "Take photo"

        self.fields['observations'].widget = forms.TextInput()
        self.fields['observations'].widget.attrs = {'class': 'form-control', 'placeholder': self.fields['observations'].label}

        self.fields['date_and_time'].widget = forms.HiddenInput()
