from django.contrib import admin
from .models import Animal, Tag

# Register your models here.


@admin.register(Animal)
class animalAdmin(admin.ModelAdmin):
    fieldsets = [
        ("Animal information", {"fields": ["animal_tag", "first_sighting", "description"]}),
        ("Animal position", {"fields": ["first_latitude", "first_longitude"]}),
    ]

    def lat_and_lon(self, obj):
        return obj.get_lat_and_lon()

    lat_and_lon.short_description = "first seen on"

    list_display = ("animal_tag", "first_sighting", "lat_and_lon", "pk",)
    list_filter = ("first_sighting",)


@admin.register(Tag)
class tagAdmin(admin.ModelAdmin):
    fieldsets = [
        ("Animal Tag", {"fields": ["animal", ]}),
        ("Tag Information", {"fields": ["date_and_time", "latitude", "longitude", "observations", "thumbnail", ]}),
    ]

    def tag_lat_and_lon(self, obj):
        return obj.get_lat_and_lon()

    tag_lat_and_lon.short_description = "Seen on"

    list_display = ("animal", "date_and_time", "tag_lat_and_lon",)
    list_filter = ("date_and_time",)
