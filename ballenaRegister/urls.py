from django.conf.urls import url
import views

urlpatterns = [
    url(r'^$', views.Index.as_view(), name='landing-page'),
    url(r'add/animal/$', views.CreateAnimal.as_view(), name='register-animal'),
    url(r'add/tag/$', views.CreateTag.as_view(), name='register-tag'),
    url(r'add/animal/(?P<animal_id>\d+)/tag/', views.CreateTag.as_view(), name='register-tag-id'),
    url(r'delete/animal/(?P<pk>\d+)', views.DeleteAnimal.as_view(), name='remove-animal'),
    url(r'delete/animal/tag/(?P<pk>\d+)', views.DeleteTag.as_view(), name='remove-tag'),
    url(r'view/$', views.AnimalsListView.as_view(), name='list-animals'),
    url(r'view/animal/(?P<pk>\d+)/$', views.DetailAnimal.as_view(), name='detail-animal'),
    url(r'view/animal/(?P<animal_id>\d+)/tags/', views.TagListView.as_view(), name='list-animal-tags'),
    url(r'view/animal/tag/(?P<animal_id>\d+)', views.DetailTag.as_view(), name='detail-tag'),
]
