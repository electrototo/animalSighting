from __future__ import unicode_literals

import os
import shutil

from django.db import models
from django.db.models.signals import pre_delete, post_save
from django.dispatch import receiver
from django.conf import settings

def change_thumbnail_name(instance, filename):
    return '{}/thumbnail/{}'.format(instance.animal_tag, filename)


def extension(fileInput):
    name, extension = os.path.splitext(fileInput)
    return extension


def change_tag_thumbnail_name(instance, filename):
    return '{}/tags/{}{}{}'.format(instance.animal.animal_tag,
                                    instance.date_and_time.date(), instance.date_and_time.time(), extension(filename))

# Create your models here.


class Animal(models.Model):
    animal_tag = models.CharField(max_length=50, unique=True)
    first_sighting = models.DateTimeField()
    description = models.TextField(blank=True)

    first_latitude = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)
    first_longitude = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)

    thumbnail = models.ImageField(blank=True, null=True, upload_to=change_thumbnail_name)

    def __str__(self):
        return self.animal_tag

    def get_lat_and_lon(self):
        data = ""

        if self.first_latitude and self.first_longitude:
            data = "{}, {}".format(self.first_latitude, self.first_longitude)
        else:
            data = "No position registered"

        return data


class Tag(models.Model):
    animal = models.ForeignKey(Animal, on_delete=models.CASCADE)
    date_and_time = models.DateTimeField()

    latitude = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)
    longitude = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)

    observations = models.TextField(blank=True)

    thumbnail = models.ImageField(blank=True, null=True, upload_to=change_tag_thumbnail_name)

    def __str__(self):
        return "Tag for: {}".format(self.animal.animal_tag)

    def get_lat_and_lon(self):
        data = ""

        if self.latitude and self.longitude:
            data = "{}, {}".format(self.latitude, self.longitude)
        else:
            data = "No position registered"

        return data

    def get_observations(self):
        if self.observations:
            return self.observations
        else:
            return "No observations found"

@receiver(pre_delete, sender=Animal)
def delete_media_of_model(sender, instance, **kwargs):
    animal_media_path = os.path.join(settings.MEDIA_ROOT, instance.animal_tag)
    shutil.rmtree(animal_media_path)

@receiver(post_save, sender=Animal)
def create_media_of_model(sender, instance, **kwargs):
    if not os.path.exists(os.path.join(settings.MEDIA_ROOT, instance.animal_tag)):
        os.makedirs(os.path.join(settings.MEDIA_ROOT, instance.animal_tag))
